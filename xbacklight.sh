#!/bin/bash

xbacklight -set $1
echo "Brightness set to $(xbacklight -get)%"
