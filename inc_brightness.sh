#!/bin/bash

xbacklight -inc 1
echo "Brightness increased to $(xbacklight -get)%"
