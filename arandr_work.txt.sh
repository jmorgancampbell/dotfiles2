#!/bin/sh
xrandr \
    --output DP-1-2-1-1 --off \
    --output DP-1-2-8 --mode 1920x1200 --pos 0x0 --rotate normal \
    --output DP-2-1 --off \
    --output DP-2-2 --off \
    --output DP-2-3 --off \
    --output DP-1-2-1-1-8 --mode 1920x1200 --pos 3840x0 --rotate normal \
    --output DP-1-2-1 --off \
    --output eDP-1 --off \
    --output DP-1-2 --off \
    --output DP-1-2-1-8 --primary --mode 1920x1200 --pos 1920x0 --rotate normal \
    --output HDMI-2 --off \
    --output HDMI-1 --off \
    --output DP-1 --off \
    --output DP-1-3 --off \
    --output DP-2 --off \
    --output DP-1-1 --off \
    --output DP-1-2-1-1-1 --off
