#!/bin/bash

xbacklight -dec 1
echo "Brightness decreased to $(xbacklight -get)%"
